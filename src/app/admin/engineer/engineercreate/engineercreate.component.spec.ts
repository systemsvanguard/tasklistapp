import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineercreateComponent } from './engineercreate.component';

describe('EngineercreateComponent', () => {
  let component: EngineercreateComponent;
  let fixture: ComponentFixture<EngineercreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EngineercreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineercreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
