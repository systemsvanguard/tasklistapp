import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ManualTasksService } from '../services/manual.service';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css']
})
export class ResourcesComponent implements OnInit {
  public Editor = ClassicEditor;
  
  resourceContent = ''
  constructor(private msvc: ManualTasksService) { }

  eng = JSON.parse(sessionStorage.getItem('currentUser'))[0].EngCode
  
  ngOnInit(): void {

    this.msvc.getResources(this.eng).subscribe(data=> {

     
      if(data.IsAdmin) this.resourceContent = data.ResourcesTabAdminContent
      else this.resourceContent = data.ResourcesTabContent
      
    })

  }

}
