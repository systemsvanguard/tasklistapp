import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Etl28daysanalysisComponent } from './etl28daysanalysis.component';

describe('Etl28daysanalysisComponent', () => {
  let component: Etl28daysanalysisComponent;
  let fixture: ComponentFixture<Etl28daysanalysisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Etl28daysanalysisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Etl28daysanalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
