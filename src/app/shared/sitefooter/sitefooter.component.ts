import { Component, OnInit } from '@angular/core';
import {formatDate} from '@angular/common'; 

@Component({
  selector: 'app-sitefooter',
  templateUrl: './sitefooter.component.html',
  styleUrls: ['./sitefooter.component.css']
})
export class SitefooterComponent implements OnInit {

  curentYear: number = Date.now();

  dateToday : number = Date.now();
  dateYesterday: number = Date.now() -  ( 1000*60*60*24  ) ;
  myDate : any = formatDate(Date.now() -  ( 1000*60*60*24  ), 'yyyy-MM-dd', 'en');
  // console.log(this.dateYesterday);



  constructor() { }

  ngOnInit(): void {
  }

}
