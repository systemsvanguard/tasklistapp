import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineerdetailComponent } from './engineerdetail.component';

describe('EngineerdetailComponent', () => {
  let component: EngineerdetailComponent;
  let fixture: ComponentFixture<EngineerdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EngineerdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineerdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
