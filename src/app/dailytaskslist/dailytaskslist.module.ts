import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { DailytaskslistRoutingModule } from './dailytaskslist-routing.module';
import { DailytaskslistComponent } from './dailytaskslist.component';
import { MatExpansionModule} from '@angular/material/expansion';
import { AutomatedTasksComponent } from '../automated-tasks/automated-tasks.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule} from '@angular/material/icon';
import { MatGridListModule} from '@angular/material/grid-list';
import { ManualTasksComponent } from '../manual-tasks/manual-tasks.component';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ResourcesComponent } from '../resources/resources.component';
import { ActivitylogComponent } from '../activitylog/activitylog.component';
import { MatListModule} from '@angular/material/list';
import { MatBadgeModule} from '@angular/material/badge';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatButtonModule} from '@angular/material/button';
import { ClipboardModule} from '@angular/cdk/clipboard';
import { ChartsModule } from 'ng2-charts';

import { SitefooterComponent } from "../shared/sitefooter/sitefooter.component";
// import { SitefooterComponent } from "src/app/shared/sitefooter/sitefooter.component";
import { BargraphsummaryComponent } from "../shared/bargraphsummary/bargraphsummary.component";
import { Etl28daysanalysisComponent } from "../etl28daysanalysis/etl28daysanalysis.component";


@NgModule({
  declarations: [
    DailytaskslistComponent,
    AutomatedTasksComponent,
    ManualTasksComponent,
    ResourcesComponent, 
    SitefooterComponent , 
    BargraphsummaryComponent, 
    Etl28daysanalysisComponent , 
    ActivitylogComponent
    
    
  ],
  imports: [
    CommonModule,
    DailytaskslistRoutingModule,
    MatExpansionModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatGridListModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    CKEditorModule,
    MatListModule,
    MatBadgeModule,
    MatPaginatorModule,
    MatButtonModule,
    ClipboardModule,
    ChartsModule      
  ],
  exports: [
  MatTabsModule, 
  MatFormFieldModule, 
  MatInputModule 
  ],
  providers:[DatePipe]
})
export class DailytaskslistModule { }
