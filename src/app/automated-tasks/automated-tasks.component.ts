import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AutomatedService } from '../services/automatedtasks.service';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FlexLayoutModule } from '@angular/flex-layout';
import {formatDate} from '@angular/common'; 

@Component({
  selector: 'app-automated-tasks',
  templateUrl: './automated-tasks.component.html',
  styleUrls: ['./automated-tasks.component.css'],
})
export class AutomatedTasksComponent implements OnInit, AfterViewInit {
  // API examples : Left side - HH by PA | http://localhost:1339/api/engtasks/incomplete/ama/20211210  
  // API examples : Right  side - Tasks by HH | http://localhost:1339/api/household/ama/7090248/2021-12-10   
  @ViewChild('paginator', { static: true }) paginator: MatPaginator;  
  @ViewChild('paginatorcomplete', { static: true })
  paginatorcomplete: MatPaginator;
  @ViewChild('paginatorflagged', { static: true })
  paginatorflagged: MatPaginator;

  displayedColumns: string[] = ['HomeNumber']; 
  displayedColumns1: string[] = ['TaskName', 'Done'];
  resultslength = 0;
  comment = '';
  eng = JSON.parse(sessionStorage.getItem('currentUser'))[0].EngCode;
  lastDate = 'Never';
  completedcount: any;
  incompletecount: any;
  flaggedcount: any;
  incomplete: MatTableDataSource<any>;
  complete: MatTableDataSource<any>;
  flagged: MatTableDataSource<any>;
  todayDate: any;
  resultslengthflag = 0;
  day = new Date();
  hhnumber: any;
  currentHH: any;
  currentHHName: any;
  // dateForAPI  : any = formatDate(Date.now() -  ( 1000*60*60*24  ), 'yyyy-MM-dd', 'en');  // used in 14 places below 
  dateForAPI : any = '2021-12-10' ;  // this page's API needs a specific date, the latest available Task Date, to run.   Example 2021-12/07 
  currentCompletedTasks: any;
  // currentCompletedTasks: any = 4;
  currentTotalTasks: any ;
  flaggedhh: any;
  priority: any;
  i : any = 1  ; 

  // search filter package "ng2-search-filter": https://www.npmjs.com/package/ng2-search-filter  | no longer relevant; uses ngFor 
  // searchterm = '';

  public doFilter = (value: string) => {
    this.incomplete.filter = value.trim().toLocaleLowerCase();
    this.complete.filter = value.trim().toLocaleLowerCase();
    this.flagged.filter = value.trim().toLocaleLowerCase();
    // this.dataSource.filter = value.trim().toLocaleLowerCase();
  }
  

  @Output() newItemEvent = new EventEmitter<any>();

  constructor(public datePiper: DatePipe, private msvc: AutomatedService) {
    this.todayDate = this.datePiper.transform(this.day, 'yyyy-MM-dd');
  }

  ngAfterViewInit() {}

  ngOnInit(): void {
    // this.msvc.getIncompleteTasks(this.eng, '2021-12-10' ).subscribe((data) => { 
    this.msvc.getIncompleteTasks(this.eng, this.dateForAPI ).subscribe((data) => {
      this.incomplete = new MatTableDataSource<any>(data);
      this.resultslength = data.length;
      console.log(this.dateForAPI);
      this.incomplete.paginator = this.paginator;

      
      this.msvc
        // .getHHTasks(this.eng, '2021-12-10', data[0].HomeNumber)  // prior code 
        .getHHTasks(this.eng, this.dateForAPI, data[0].HomeNumber)
        .subscribe((data) => {
          this.currentHH = data[0].HomeNumber;
          this.currentHHName = data[0].HHName;
          // this.currentCompletedTasks = 0;
          console.log(this.dateForAPI);
          //console.log(data[0].CompletedTasks);
          // console.log(this.currentCompletedTasks);
          this.currentTotalTasks = data.length ;     
          this.flaggedhh = data[0].HouseholdFlag;          
          //this.currentCompletedTasks = 1 ;
          // this.currentTotalTasks = 2  ;          
          // this.currentCompletedTasks = data[0].CompletedTasks.toString() ;
          // this.currentTotalTasks = String(data[0].TotalTasks) ;
          this.hhnumber = data;
          this.flaggedhh = data[0].HouseholdFlag;
          this.priority  = data[0].Priority;
        });

      this.msvc
        // .getHHComments(this.eng, '2021-12-10', data[0].HomeNumber) 
        .getHHComments(this.eng, this.dateForAPI, data[0].HomeNumber) 
        .subscribe((data) => {
          if (data != undefined) this.comment = data.Comments;
        });
    });

    // this.msvc.getCompleteTasks(this.eng, '2021-12-10').subscribe((data) => { 
    this.msvc.getCompleteTasks(this.eng, this.dateForAPI).subscribe((data) => {
      this.complete = new MatTableDataSource<any>(data);
      this.resultslength = data.length;
      console.log(data);
      this.complete.paginator = this.paginatorcomplete;
    });

    // this.msvc.getFlaggedTasks(this.eng, '2021-12-10').subscribe((data) => { 
    this.msvc.getFlaggedTasks(this.eng, this.dateForAPI).subscribe((data) => {
      this.flagged = new MatTableDataSource<any>(data);
      this.resultslengthflag = data.length;
      console.log(data);
      this.flagged.paginator = this.paginatorflagged;
      if (data != undefined) this.flaggedhh = true;
    });

    // this.msvc.getDashboard(this.eng, '2021-12-10').subscribe((data) => { 
    this.msvc.getDashboard(this.eng, this.dateForAPI).subscribe((data) => {
      this.incompletecount = data.UnstartedByHH;
      this.completedcount = data.CompletedByHH;
      this.flaggedcount = data.FlaggedByHH;
      this.newItemEvent.emit(data);
      console.log(data);
    });
  }

  showDetails(hh: number) {
    // this.msvc.getHHTasks(this.eng, '2021-12-10', hh).subscribe((data) => { 
    this.msvc.getHHTasks(this.eng, this.dateForAPI, hh).subscribe((data) => {
      this.currentHH = data[0].HomeNumber;
      this.currentHHName = data[0].HHName;
      this.hhnumber = data;
      this.resultslengthflag = data.length;
      this.flaggedhh = data[0].Priority;
      this.priority = data[0].HouseholdFlag;
      this.currentCompletedTasks = data[0].CompletedTasks ;
      this.currentTotalTasks = data.length; 
      // this.resultslengthflag = data.length;
      console.log(data[0]);
    });

    this.msvc
      // .getHHComments(this.eng, '2021-12-10', this.currentHH) 
      .getHHComments(this.eng, this.dateForAPI, this.currentHH)
      .subscribe((data) => {
        if (data != undefined) this.comment = data.Comments;
      });
  }

  updateComment() {
    this.msvc
      // .updateHHComments(this.eng, '2021-12-10', this.currentHH, this.comment) 
      .updateHHComments(this.eng, this.dateForAPI, this.currentHH, this.comment)
      .subscribe((data) => {});
  }
  updatehhDetails(hh: any) {
    // this.msvc.updateHHDetails(this.eng, '2021-12-10', hh).subscribe((data) => { 
    this.msvc.updateHHDetails(this.eng, this.dateForAPI, hh).subscribe((data) => {
      this.ngOnInit();
    });
  }

  updatehhFlagged(hh: any) {
    this.msvc
      // .updateHHFlagged(this.eng, '2021-12-10', hh, this.flaggedhh)  
      .updateHHFlagged(this.eng, this.dateForAPI, hh, this.flaggedhh)
      .subscribe((data) => {
        this.ngOnInit();
      });
  }
}
