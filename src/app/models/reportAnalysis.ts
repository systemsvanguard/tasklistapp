export class ReportAnalysis {

    EngDailyReportAnalysisID: number;
    NeedsAnalysis: boolean;
    Completed: boolean;
}