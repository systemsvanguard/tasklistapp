import { HttpClient,HttpHeaders, HttpParams, } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ManualInfo } from '../models/manualTasks';
import { ReportAnalysis } from '../models/reportAnalysis';
import { UserInfo } from '../models/userDetails';

@Injectable({
  providedIn: 'root'
})
export class ManualTasksService {


  
  constructor(private http: HttpClient) { 

 
  }

getManualTasks(engcode:string,taskdate:string) {

    return this.http.get<ManualInfo>(`${environment.apiurl}/manual/tasks/${engcode}/${taskdate}`)
    .pipe(map(data => {
        
        return data;
    }) )
}

getReport(engcode:string,taskdate:string) {

  return this.http.get<[ReportAnalysis]>(`${environment.apiurl}/manual/report/${engcode}/${taskdate}`)
  .pipe(map(data => {
      
      return data[0];
  }) )
}

getComments(engcode:string,taskdate:string) {

  return this.http.get<any>(`${environment.apiurl}/manual/comments/${engcode}/${taskdate}`)
  .pipe(map(data => {
    if(data.length >0)
      return data[0].Comments
      else
      return '';
     
  }) )
}

getActivityLog(engcode:string,taskdate:string) {

  return this.http.get<any>(`${environment.apiurl}/manual/activitylog/${engcode}/${taskdate}`)
  .pipe(map(data => {
      
   
    return data[0];
   
      
  }) )
}

getResources(engcode:string) {

  return this.http.get<any>(`${environment.apiurl}/manual/resources/${engcode}`)
  .pipe(map(data => {
      
      return data[0];
  }) )
}

updateReport(item:any ,engcode:string,taskdate:string) {

  return this.http.post<any>(`${environment.apiurl}/manual/updateReport/${engcode}/${taskdate}`,{
    EngDailyReportAnalysisID: (item.EngDailyReportAnalysisID == undefined) ? 0 :item.EngDailyReportAnalysisID,
    NeedsAnalysis:(item.NeedsAnalysis == true) ? 1:0,
    Completed:(item.Completed == true) ?1:0
    
  }).pipe(map(data=>{
    
    return data;
  }))
}

updateComment(engcode:string,taskdate:string,comment:string) {

  return this.http.post<any>(`${environment.apiurl}/manual/updateComments/${engcode}/${taskdate}`,{
    EngCode: engcode,
    Comments:comment
    
  }).pipe(map(data=>{
    
    return data;
  }))
}

UpdateCount(item:any,engcode:string) {

  return this.http.post<any>(`${environment.apiurl}/manual/updateTasks/${engcode}`,{
    EngDailyManualTaskID: item.EngDailyManualTaskID,
    CompletedItems:item.CompletedItems,
    TotalItems: item.TotalItems
  })
  .pipe(map(data => {
    
    return data;
}) )


}

}
