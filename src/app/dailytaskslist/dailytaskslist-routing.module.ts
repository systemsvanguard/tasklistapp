import { animation } from '@angular/animations';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { DailytaskslistComponent } from './dailytaskslist.component';

const routes: Routes = [{ path: '', component: DailytaskslistComponent ,data: {animation: 'dailytasks'} }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DailytaskslistRoutingModule { }
